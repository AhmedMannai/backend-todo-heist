import { IsEmail, IsString } from 'class-validator';

export class EditUserDto {
  @IsString()
  firstName: string;
  @IsString()
  lastName: string;

  @IsEmail()
  @IsString()
  email: string;
}
