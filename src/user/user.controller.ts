import {
  Body,
  Controller,
  Get,
  Patch,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/common/guards/at.guard';
import { UserService } from './user.service';
import { EditUserDto } from './edit-user.dto';
import { GetCurrentUserId } from 'src/common/decorator';

@UseGuards(JwtAuthGuard)
@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('me')
  getMe(@GetCurrentUserId() userId: number) {
    return this.userService.getMe(userId);
  }

  @Put('me/edit')
  editUser(@GetCurrentUserId() userId: number, @Body() user: EditUserDto) {
    return this.userService.editMe(userId, user);
  }
}
