import { Module } from '@nestjs/common';
import { TaskListController } from './todo-list.controller';
import { TodoListService } from './todo-list.service';

@Module({
  providers: [TodoListService, TodoListService],
  controllers: [TaskListController],
})
export class TaskListModule {}
