import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/common/guards';
import { TodoListService } from './todo-list.service';
import { CreateTodoListDto } from './dto';
import { GetCurrentUserId } from 'src/common/decorator';

@UseGuards(JwtAuthGuard)
@Controller('todoLists')
export class TaskListController {
  // apis for task-list
  constructor(private todoListService: TodoListService) {}

  @Get()
  getTodoLists(@GetCurrentUserId() userId: number) {
    console.log(`user id: ${userId}`);
    return this.todoListService.getTodoLists(userId);
  }

  @Get(':id')
  getTodoListById(
    @GetCurrentUserId() userId: number,
    @Param('id', ParseIntPipe) todoListId: number,
  ) {
    return this.todoListService.getTodoById(userId, todoListId);
  }

  @Post()
  createTodoList(
    @GetCurrentUserId() userId: number,
    @Body() dto: CreateTodoListDto,
  ) {
    console.log(`user id: ${userId} dto: ${JSON.stringify(dto)}`);
    return this.todoListService.createTodoList(userId, dto);
  }

  @Post(':id')
  editTodoList(
    @GetCurrentUserId() userId: number,
    @Body() dto: CreateTodoListDto,
    @Param('id', ParseIntPipe) todoListId: number,
  ) {
    console.log(dto);
    return this.todoListService.editTodoList(userId, todoListId, dto);
  }

  @Get(':id/delete')
  deleteTodoList(
    @GetCurrentUserId() userId: number,
    @Param('id', ParseIntPipe) todoListId: number,
  ) {
    return this.todoListService.deleteTodoList(userId, todoListId);
  }
}
