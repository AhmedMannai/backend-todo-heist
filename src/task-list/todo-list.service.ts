import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateTodoListDto } from './dto/todoList.dto';

@Injectable()
export class TodoListService {
  constructor(private prisma: PrismaService) {}

  async createTodoList(userId: number, dto: CreateTodoListDto) {
    const todoList = await this.prisma.todoList.create({
      data: {
        userId: userId,
        ...dto,
      },
    });
    return todoList;
  }

  async getTodoLists(userId: number) {
    const todoLists = await this.prisma.todoList.findMany({
      where: {
        userId: userId,
      },
    });
    return todoLists;
  }

  getTodoById(userId: number, taskListId: number) {
    return this.prisma.todoList.findFirst({
      where: {
        id: taskListId,
        userId,
      },
    });
  }

  async editTodoList(
    userId: number,
    taskListId: number,
    dto: CreateTodoListDto,
  ) {
    const todoList = await this.prisma.todoList.update({
      where: {
        id: taskListId,
      },
      data: {
        ...dto,
        user: {
          connect: { id: userId },
        },
      },
    });
    return todoList;
  }

  async deleteTodoList(userId: number, taskListId: number) {
    const todoList = await this.prisma.todoList.findUnique({
      where: {
        id: taskListId,
      },
    });

    // check if user owns the bookmark
    if (!todoList || todoList.userId !== userId) {
      throw new ForbiddenException('Access to resource denied');
    }
    await this.prisma.todoList.delete({
      where: {
        id: taskListId,
      },
    });
  }
}
