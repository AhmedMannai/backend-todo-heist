import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/common/guards';
import { TodoService } from './todo.service';
import { GetCurrentUserId } from 'src/common/decorator';
import { CreateTodoDto } from './dto/create-todo.dto';

@UseGuards(JwtAuthGuard)
@Controller('todoLists/:todolistID/todos')
export class TodoController {
  constructor(private todoService: TodoService) {}

  @Post()
  async createTodo(
    @GetCurrentUserId() userId: number,
    @Param('todolistID', ParseIntPipe) todoListId: number,
    @Body() dto: CreateTodoDto,
  ) {
    console.log(dto);
    return this.todoService.createTodo(userId, todoListId, dto);
  }

  @Get()
  async getAllTodosForTodoList(
    @GetCurrentUserId() userId,
    @Param('todolistID', ParseIntPipe) todoListId: number,
  ) {
    return this.todoService.getAllTodosForTodoList(userId, todoListId);
  }

  @Get(':todoID')
  async getTodo(
    @GetCurrentUserId() userId,
    @Param('todoID', ParseIntPipe) todoId: number,
    @Param('todolistID', ParseIntPipe) todoListId: number,
  ) {
    return this.todoService.getTodoById(userId, todoListId, todoId);
  }

  @Post(':todoID/update')
  async editTodo(
    @GetCurrentUserId() userId: number,
    @Param('todoID', ParseIntPipe) todoId: number,
    @Param('todolistID', ParseIntPipe) todoListId: number,
    @Body() dto: CreateTodoDto,
  ) {
    return this.todoService.editTodo(userId, todoListId, todoId, dto);
  }

  @Get(':todoID/delete')
  async deleteTodo(
    @GetCurrentUserId() userId: number,
    @Param('todoID', ParseIntPipe) todoId: number,
    @Param('todolistID', ParseIntPipe) todoListId: number,
  ): Promise<boolean> {
    return this.todoService.deleteTodo(userId, todoListId, todoId);
  }

  // @Post(':todoID/toggle')
  // async toggleTodo(
  //   @GetCurrentUserId() userId: number,
  //   @Param('todoID', ParseIntPipe) todoId: number,
  //   @Param('todolistID', ParseIntPipe) todoListId: number,
  // ) {
  //   return this.todoService.toggleTodoStatus(userId, todoListId, todoId);
  // }
}
