import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class EditTodoDto {
  @IsString()
  @IsNotEmpty()
  content?: string;

  @IsBoolean()
  isDone? = false;
}
