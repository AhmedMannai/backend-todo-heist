import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateTodoDto } from './dto/create-todo.dto';
import { EditTodoDto } from './dto/edit-todo.dto';

@Injectable()
export class TodoService {
  constructor(private prisma: PrismaService) {}

  async createTodo(userId: number, todoListId: number, dto: CreateTodoDto) {
    const todoList = await this.prisma.todoList.findFirst({
      where: {
        id: todoListId,
        userId: userId,
      },
    });

    if (!todoList) {
      throw new ForbiddenException(
        'There is no todo list with this id for this user',
      );
    }

    const todo = await this.prisma.todo.create({
      data: {
        ...dto,
        todoList: { connect: { id: todoListId } },
      },
    });

    return todo;
  }

  async getTodoById(userId: number, todoListId: number, todoId: number) {
    const todo = await this.prisma.todo.findFirst({
      where: {
        id: todoId,
        todoList: {
          id: todoListId,
          userId: userId,
        },
      },
    });

    if (!todo) {
      throw new ForbiddenException(
        'There is no todo with this id for this user',
      );
    }

    return todo;
  }

  async getAllTodosForTodoList(userId: number, todoListId: number) {
    const todoList = this.prisma.todoList.findUnique({
      where: { id: todoListId },
    });

    if (!todoList) {
      throw new ForbiddenException(
        'There is no todo list with this id for this user',
      );
    }

    const todos = this.prisma.todo.findMany({
      where: {
        todoList: {
          id: todoListId,
          userId: userId,
        },
      },
    });
    return todos;
  }

  async editTodo(
    userId: number,
    todoListId: number,
    todoId: number,
    dto: EditTodoDto,
  ) {
    const todoList = this.prisma.todoList.findFirst({
      where: { userId, id: todoListId },
    });

    if (!todoList) {
      throw new ForbiddenException(
        'There is no todo list with this id for this user',
      );
    }

    const todo = this.prisma.todo.findFirst({
      where: { id: todoId, todoList: { id: todoListId } },
    });

    if (!todo) {
      throw new ForbiddenException('todo does note exist');
    }

    const editedTodo = await this.prisma.todo.update({
      where: {
        id: todoId,
      },
      data: {
        ...dto,
      },
    });
    return editedTodo;
  }

  // deleteTodo
  async deleteTodo(userId, todoListId, todoId): Promise<boolean> {
    const todoList = this.prisma.todoList.findFirst({
      where: { userId, id: todoListId },
    });

    if (!todoList) {
      throw new ForbiddenException(
        'There is no todo list with this id for this user',
      );
    }

    this.prisma.todo.delete({
      where: {
        id: todoId,
      },
    });
    return true;
  }
}
