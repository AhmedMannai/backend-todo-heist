import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { AuthService } from './auth.service';
import { AuthDto } from './dto';
import { JWTPayload, Tokens } from './types';
import { GetCurrentUserId, Public } from 'src/common/decorator';
import { RtGuard } from 'src/common/guards';
import { GetCurrentUser } from 'src/common/decorator/get-current-user';
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @Post('signup')
  @HttpCode(HttpStatus.CREATED)
  async signup(@Body() dto: AuthDto): Promise<Tokens> {
    return this.authService.signup(dto);
  }

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('signin')
  async signin(@Body() dto: AuthDto): Promise<Tokens> {
    return this.authService.signin(dto);
  }

  @Public()
  @Post('logout')
  @HttpCode(HttpStatus.OK)
  @UseGuards(RtGuard)
  async logout(@Req() req: Request): Promise<boolean> {
    const userData = req.user as JWTPayload;
    console.log(`\nlogout.... \n${userData}`);

    return this.authService.logout(userData['sub']);
  }

  @Get('verify')
  @HttpCode(HttpStatus.OK)
  async verify() {
    console.log('token is valid');
    return true;
  }

  @Public()
  @UseGuards(RtGuard)
  @Post('refresh')
  @HttpCode(HttpStatus.OK)
  async refresh(
    @GetCurrentUserId() userId: number,
    @GetCurrentUser('refreshToken') refreshToken: string,
  ): Promise<Tokens> {
    return this.authService.refreshTokens(userId, refreshToken);
  }
}
