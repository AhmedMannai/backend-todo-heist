import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { JWTPayload } from '../types';

export const GetCurrentUserId = createParamDecorator(
  (_: undefined, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user as JWTPayload;
    return user.sub;
  },
);
