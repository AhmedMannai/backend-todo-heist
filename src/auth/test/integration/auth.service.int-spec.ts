import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import { AuthService } from 'src/auth/auth.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { Tokens } from 'src/auth/types';

const dto = {
  email: 'test1@gmail.com',
  password: 'super-secret-password',
  firstName: 'integration',
  lastName: 'test',
};

describe('AuthService Int', () => {
  let prisma: PrismaService;
  let authService: AuthService;
  let moduleRef: TestingModule;

  beforeAll(async () => {
    moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    prisma = moduleRef.get(PrismaService);
    authService = moduleRef.get(AuthService);
  });

  afterAll(async () => {
    await moduleRef.close();
    // await prisma.cleanDatabase();
  });

  describe('siginup', () => {
    beforeAll(async () => {
      await prisma.cleanDatabase();
    });

    it('should siginup a user', async () => {
      const tokens = await authService.signup({
        email: dto.email,
        password: dto.password,
        firstName: dto.firstName,
        lastName: dto.lastName,
      });

      expect(tokens.access_token).toBeTruthy();
    });

    it('should throw on duplicated user signup', async () => {
      let tokens: Tokens | undefined;

      try {
        tokens = await authService.signup({
          email: dto.email,
          password: dto.password,
          firstName: dto.firstName,
          lastName: dto.lastName,
        });
      } catch (error) {
        expect(error.status).toBe(403);
      }
      expect(tokens).toBeUndefined();
    });
  });

  describe('signin', () => {
    beforeAll(async () => {
      await prisma.cleanDatabase();
      // await authService.signup(dto);
    });

    it('should throw on unexist user', async () => {
      let tokens: Tokens | undefined;

      try {
        tokens = await authService.signin({
          email: '1' + dto.email,
          password: dto.password,
        });
      } catch (error) {
        expect(error.status).toBe(403);
      }
      expect(tokens).toBeUndefined();
    });

    it('throw on wrong password', async () => {
      let tokens: Tokens | undefined;

      try {
        tokens = await authService.signin({
          email: dto.email,
          password: dto.password + '1',
        });
      } catch (error) {
        expect(error.status).toBe(403);
      }
      expect(tokens).toBeUndefined();
    });

    it('should signin a user', async () => {
      const signup_token = await authService.signup(dto);

      const tokens = await authService.signin({
        email: dto.email,
        password: dto.password,
      });

      expect(signup_token).toBeTruthy();
      expect(tokens.access_token).toBeTruthy();
    });
  });
});
