import { JWTPayload } from './jwtPayload';

export type JWTPayloadWithRT = JWTPayload & { refreshToken: string };
